/*
 * 	iPLMVSMReport.java
 ********************************************************************************************
 * Modification Details:
 *
 * Ver|  Date       | CDSID    | CR      | Comment
 * ---|-------------|----------|---------|--------------------------------------------------
 * 01 | 04-FEB-2019 | rkakde   | 18192   | Modified/Added methods
 ********************************************************************************************
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import java.util.TreeMap;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.matrixone.apps.domain.DomainConstants;
import com.matrixone.apps.domain.DomainObject;
import com.matrixone.apps.domain.util.ContextUtil;
import com.matrixone.apps.domain.util.FrameworkUtil;
import com.matrixone.apps.domain.util.MapList;
import com.matrixone.apps.domain.util.MqlUtil;
import com.matrixone.apps.domain.util.PropertyUtil;
import com.matrixone.apps.domain.util.StringUtil;
import com.matrixone.apps.framework.ui.UIUtil;

import matrix.db.Context;
import matrix.db.JPO;
import matrix.util.MatrixException;
import matrix.util.Pattern;
import matrix.util.StringList;
import sun.misc.BASE64Decoder;

import java.util.Comparator;
import java.util.regex.Matcher;

public class iPLMVSMReport {
	
	public static String sNoCASURL = null;
	public static String sUserName = null;
	public static String sPassword = null;
	public static String sInputFilePath = null;
	public static String sConfigurationFilePath = null;
	public static String sLogPath = null;
	public static String sLDIProductMaturity = null;
	public static String sReportPath = null;
	
	public static Logger logger = Logger.getLogger("VSMReportLog");
	
	public static Map mpGFD_ConfFeatures = new LinkedHashMap();
	public static StringList slInputFile = new StringList();
	//Added for Incident 17457 : rkakde : Start
	public static Map mapModelYear = new HashMap();
	public static Map mpVSMDetail = new HashMap();
	public static final java.util.regex.Pattern p = java.util.regex.Pattern.compile("\\d+");
	//Added for Incident 17457 : rkakde : End
	
	private static final byte[] SPEC = {
									-34, 51, 16, 18,
									-34, 51, 16, 18 };
	
	public iPLMVSMReport(String[] args)
	{
		sConfigurationFilePath = args[0].trim();
		sInputFilePath = args[1].trim();
		File file = new File(sConfigurationFilePath);
		Properties properties = new Properties();
		try
		{
			FileInputStream fileInput = new FileInputStream(file);
			properties.load(fileInput);
			fileInput.close();
			sNoCASURL = properties.getProperty("URL");
			sUserName = properties.getProperty("UserName");
			sPassword = properties.getProperty("Password");
			sLogPath = properties.getProperty("LogDirectory");			
			sLDIProductMaturity = properties.getProperty("LDI_Product_Maturity");			
			sReportPath = properties.getProperty("ReportPath");
			FileHandler fh = new FileHandler(sLogPath, true);
			logger.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
			
		}catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	private Context setContext(String sUserName, String sPassword, String sServerPath)
	{
		Context context = null;
		try
		{
			context = new Context(sServerPath);
			context.setUser(sUserName);
			sPassword = decodeString(sPassword);
			context.setPassword(sPassword);
			context.connect();
			//context.start(true);
			logger.info("setContext Success...");
		}
		catch (MatrixException e)
		{
			e.printStackTrace();
		}
		return context;
	}
	
	private static String decodeString(String encryptedString)
	{
		String decodedString = "";
		encryptedString = encryptedString.substring(4, encryptedString.length() - 4);
		try
		{
			decodedString = getDecryptedData(encryptedString);
		}
		catch (GeneralSecurityException ge)
		{
			ge.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return decodedString;
	}
	
	public static String getDecryptedData(String encryptedData)
			throws GeneralSecurityException, IOException
	{
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBEWithMD5AndDES");
		char[] keySpec = "truhgsrrfhrty6yrth".toCharArray();
		SecretKey key = keyFactory.generateSecret(new PBEKeySpec(keySpec));
		Cipher pbeCipher = Cipher.getInstance("PBEWithMD5AndDES");
		pbeCipher.init(2, key, new PBEParameterSpec(SPEC, 20));

		byte[] bytes = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] finalBytes = pbeCipher.doFinal(bytes);
		String decryptedData = new String(finalBytes, "UTF-8");
		return decryptedData;
	}

	public static void main(String[] args) {
		iPLMVSMReport obj = new iPLMVSMReport(args);
		Context context = obj.setContext(sUserName, sPassword, sNoCASURL);	
		try
		{	
			obj.processInput(context);
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
	}
	
	public void processInput(Context context)throws Exception{
		try{
			
			String strInputObj = null;
			String sModelName = null;
			String sModelId = null;
			String sModelphysicalId = null;
			String[] saInputObj = null;
			//Added for Incident 17457 : rkakde : Start
			String strMY = null;
			String sVSMVal = "";
			StringList slVSMVal = new StringList();
			Map vsmMYMap = null;
			String[] saMY = null;
			String strVSMData = null;
			String strVSMCO = null;
			//Added for Incident 17457 : rkakde : End
			boolean isGeneric = true;
			slInputFile = readInputFile(sInputFilePath);
			int iInputCount = slInputFile.size();
			
			ContextUtil.pushContext(context, PropertyUtil.getSchemaProperty(context, "person_UserAgent"), "", "");
			StringList slBusSelects = new StringList();
			slBusSelects.add("id");
			slBusSelects.add("physicalid");

			Map mapFeatures = new HashMap();
			Map mapModelVSM = new HashMap();
			
			for(int i=0;i<iInputCount;i++){
				mpVSMDetail = new HashMap();
				
				strInputObj = (String)slInputFile.get(i);
				saInputObj = strInputObj.split("\\|");
				sModelName = saInputObj[0];
				
				MapList mpModel = DomainObject.findObjects(context, "Model", sModelName, "*","*","*", null, false, slBusSelects);
				 
				if ((mpModel != null) && (mpModel.size() > 0))
				{
					Map mapModel = (Map)mpModel.get(0);
					sModelId = (String)mapModel.get("id");
					sModelphysicalId = (String)mapModel.get("physicalid");

					mapFeatures = getGFDFeatureFamily(context,sModelId);
					mapModelVSM = getModelVSM_COData(context,sModelphysicalId,sLDIProductMaturity);

					Set<String> stModelYear = null;
					//Added for Incident 17457 : rkakde : Start
				
					Map treeMap = new TreeMap(mapModelYear);
					stModelYear = treeMap.keySet();
						String strVSMList = "";

					for(String strVSM : stModelYear)
					{
							sVSMVal = (String)mapModelYear.get(strVSM.trim());
							slVSMVal = FrameworkUtil.split(sVSMVal, "|");
								List<String> slVSMList = new ArrayList<String>(slVSMVal);
								java.util.Collections.sort(slVSMVal,c);
								java.util.Collections.sort(slVSMList,c);
								String strTempList = slVSMList.toString();

								//String strTemp = FrameworkUtil.join((StringList) slVSMList, ",");
								String strTemp = strTempList.replace("[","");
								strTemp = strTemp.replace("]", "");
								if(UIUtil.isNotNullAndNotEmpty(strVSMList)){
									strVSMList = strVSMList+","+strTemp;

								}else{
									strVSMList = strTemp;

								}
							vsmMYMap = new HashMap();
							for(int iVSM=0;iVSM<slVSMVal.size();iVSM++)
							{
								strVSMData = (String)slVSMVal.get(iVSM);
								strVSMData = strVSMData.trim();

								strVSMCO = (String)mapModelVSM.get(strVSMData);
								vsmMYMap.put(strVSMData, strVSMCO);
							}

								generateVSM_COData(context,mapFeatures,vsmMYMap,sModelName,strVSM,strTemp);

					}
					
						generateVSM_COData(context,mapFeatures,mapModelVSM,sModelName,"",strVSMList);
					//Added for Incident 17457 : rkakde : End

				}else{
					logger.info("Model "+sModelName+" object doesnt exists" );	
				}
			}			
		}
		catch (Exception e)
		{
			//ContextUtil.abortTransaction(context);
			logger.info("ERROR " + e);
			throw e; 
		}
		finally
		{
			ContextUtil.popContext(context);
		}
	}
	public static StringList readInputFile(String sInputFilePath) throws Exception{
		FileInputStream fis = null;
        BufferedReader reader = null;
        StringList slInputFile = new StringList();
		try 
		{
            fis = new FileInputStream(sInputFilePath);
            reader = new BufferedReader(new InputStreamReader(fis));
            String line = reader.readLine();
            while(line != null)
            {
                slInputFile.add(line);
                line = reader.readLine();
            } 
		}catch (FileNotFoundException ex) {
            Logger.getLogger(iPLMVSMReport.class.getName()).log(Level.SEVERE, null, ex);
        }catch (IOException ex) {
            Logger.getLogger(iPLMVSMReport.class.getName()).log(Level.SEVERE, null, ex);          
        } finally {
            try {
                reader.close();
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(iPLMVSMReport.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
		return slInputFile;
  }
	
	public Map getGFDFeatureFamily(Context context, String sGFDId)throws Exception{
		Map mapFeatures = new LinkedHashMap();
		try {
			String sConfFeatureId = null;
			String sConfFeatureRelId = null;
			String sConfFeatureName = null;
			String sConfFeatureDisplayName = null;
			String sConfFeatureLevel = null;
			
			DomainObject domConfFeat = DomainObject.newInstance(context);
			DomainObject gfdDomObj = new DomainObject(sGFDId);
			Pattern typePattern = new Pattern("Configuration Feature");			
			Pattern strRelPattern = new Pattern("Configuration Context");

			StringList relationshipSelects = new StringList();
			relationshipSelects.add("id[connection]");
			
			StringList strSelectableList = new StringList();
			strSelectableList.add("id");
			strSelectableList.add("type");
			strSelectableList.add("name");
			strSelectableList.add("level");
			strSelectableList.add("attribute[Display Name]");
			
			MapList mpConfigFeatData = gfdDomObj.getRelatedObjects(context, 
										strRelPattern.getPattern(),
										typePattern.getPattern(),
										strSelectableList, 
										relationshipSelects, 
										true, 
										false, 
										(short)0,
										"", 
										null,
										0);

			mpConfigFeatData.addSortKey("attribute[Display Name]", "ascending", "String");
			mpConfigFeatData.sort();
			
			int iConfigData = mpConfigFeatData.size();
				
			for(int itr=0;itr<iConfigData;itr++){
				String[] params = new String[1];
				String strActualExpression = null;
				String strDisplayExpression = null;
				String strCODisplayName = null;
				String sConfOptId = null;
				String sConfOptLevel = null;
				String sConfOptName = null;
				
				MapList mlCO = new MapList();
				Map confDetails = new HashMap();
				Map mapConfigData = (Map)mpConfigFeatData.get(itr);
				sConfFeatureId = (String)mapConfigData.get("id");
				sConfFeatureName = (String)mapConfigData.get("name");
				sConfFeatureDisplayName = (String)mapConfigData.get("attribute[Display Name]");
				sConfFeatureLevel = (String)mapConfigData.get("level");

				domConfFeat.setId(sConfFeatureId);
				confDetails.put("id", sConfFeatureId);
				confDetails.put("name", sConfFeatureName);
				confDetails.put("DisplayName", sConfFeatureDisplayName);
				confDetails.put("sConfFeatureLevel", sConfFeatureLevel);
				mpGFD_ConfFeatures.put(sConfFeatureId,confDetails);
				
				MapList mpConfigOptData = domConfFeat.getRelatedObjects(context, 
											"Configuration Options", 
											"Configuration Option", 
											strSelectableList, 
											relationshipSelects, 
											false,
											true,
											(short)0,
											"",
											"",
											0,
											null, 
											null, 
											null);
				
				mpConfigOptData.addSortKey("attribute[Display Name]", "ascending", "String");
				mpConfigOptData.sort();

				int iConfigOpData = mpConfigOptData.size();
				
				for(int coItr = 0 ; coItr<iConfigOpData; coItr++)
				{
					Map coMap = new HashMap();
					Map mapConfigOpData = (Map)mpConfigOptData.get(coItr);
					sConfOptLevel = (String)mapConfigOpData.get("level");											
					sConfOptName = (String)mapConfigOpData.get("name");											
					sConfOptId = (String)mapConfigOpData.get("id");											
					sConfFeatureRelId = (String)mapConfigOpData.get("id[connection]");		
					strCODisplayName = (String)mapConfigOpData.get("attribute[Display Name]");		
					params[0]=sConfFeatureRelId;
					Map mapExpression = JPO.invoke(context, "iPLMUtilBase", null, "getEffectivityExpresssion", params,Map.class);
					strActualExpression = (String) mapExpression.get("actualValue");
					strDisplayExpression = (String) mapExpression.get("displayValue");
					
					coMap.put("level", sConfOptLevel);
					coMap.put("name", sConfOptName);
					coMap.put("id", sConfOptId);
					coMap.put("DisplayName", strCODisplayName);
					coMap.put("ActualEffectivity", strActualExpression);
					coMap.put("DisplayEffectivity", strDisplayExpression);
					mlCO.add(coMap);			
				}
				mapFeatures.put(sConfFeatureId, mlCO);							
			}
		} catch (Exception e) {
			logger.info("ERROR " + e);
			e.printStackTrace();
		}
		return mapFeatures;
	}
	
	public Map getModelVSM_COData(Context context, String sModelId, String sLDIProductMaturity)throws Exception{
		Map mpVSMCOList  = new HashMap();
		try {
			String sLDIId = null;
			String sVSMId = null;
			String sVSMName = null;
			String sVSMDesc = null;
			//Added for Incident 17457 : rkakde : Start
			String sModelYearName = null;
			String sVal = "";
			Map mpVSMData = null;
			StringList slVSMSelects = null;
			//Added for Incident 17457 : rkakde : End
			DomainObject domLDI = DomainObject.newInstance(context);
			DomainObject domVSM = DomainObject.newInstance(context);
			
			DomainObject modelDomObj = new DomainObject(sModelId);
			
			StringList relationshipSelects = new StringList();
			relationshipSelects.add("id[connection]");
			
			StringList strSelectableList = new StringList();
			strSelectableList.add("id");
			strSelectableList.add("type");
			strSelectableList.add("name");
			strSelectableList.add("description");
		
			String strWhere = "current=='"+sLDIProductMaturity+"'";
			
			MapList mpLDIData = modelDomObj.getRelatedObjects(context, 
										"Products",
										"*",
										strSelectableList, 
										relationshipSelects, 
										false, 
										true, 
										(short)0,
										strWhere, 
										null,
										0);
			
			int iLDIData = mpLDIData.size();		
			
			StringList sVSMSelectableList = new StringList();
			sVSMSelectableList.add("id");
			sVSMSelectableList.add("name");
			sVSMSelectableList.add("description");
			sVSMSelectableList.add("relationship[Selected Options].torel[Configuration Options].to.id");
			
			for(int i=0;i<iLDIData;i++){
				String sVSMCO = null;
				Map mapLDIData = (Map)mpLDIData.get(i);
				sLDIId = (String)mapLDIData.get("id");		
				domLDI.setId(sLDIId);
				
				MapList mlVSMData = domLDI.getRelatedObjects(context, 
										"Product Configuration",
										"*",
										strSelectableList,
										relationshipSelects,
										false, 
										true, 
										(short)0,
										null, 
										null,
										0);
			
				int iVSMData = mlVSMData.size();
				MapList mlModelYear = new MapList();
				for(int itr=0;itr<iVSMData;itr++){
					mpVSMData = (Map)mlVSMData.get(itr);
					sVSMId = (String)mpVSMData.get("id");
					sVSMName = (String)mpVSMData.get("name");
					sVSMDesc = (String)mpVSMData.get("description");
					domVSM.setId(sVSMId);
					//Added for Incident 17457 : rkakde : Start 
					String[] saVSM = sVSMName.split("-");
					sModelYearName = saVSM[2]; 
					sModelYearName = sModelYearName.trim();
					//Added for Incident 17457 : rkakde : End 
					slVSMSelects = domVSM.getInfoList(context, "relationship[Selected Options].torel[Configuration Options].to.id");
					sVSMName = sVSMName.trim();
					//Added for Incident 17457 : rkakde : Start
					if(mapModelYear.containsKey(sModelYearName)){
						sVal = (String)mapModelYear.get(sModelYearName);
						sVal = sVal+"|"+sVSMName;
						mapModelYear.put(sModelYearName, sVal);
					}else{
						mapModelYear.put(sModelYearName, sVSMName);
					}
					mpVSMCOList.put(sVSMName, slVSMSelects.toString());
					mpVSMDetail.put(sVSMName, sVSMName+" ("+ sVSMDesc+")");
					
					//Added for Incident 17457 : rkakde : End
				}
			}

		} catch (Exception e) {
			logger.info("ERROR " + e);
			e.printStackTrace();
		}
		return mpVSMCOList;
	}
	
	public void generateVSM_COData(Context context, Map mapFeatures, Map mapModelVSM, String sModelName,String sModelYear,String strVSM)throws Exception{
		try {
			String strFeatureId = null;
			String strCOId = null;
			String strEffctivityEffectivity = null;
			String strChar = "";
			String strEffectIn = "";
			String strEffectOut = "";
			String strVSMDesc = "";
			String sVSMKey = "";

			String[] saEffectInAndOutPoints = null;
			String strEffectivityPoints = null;
			String sModelPhysicalId=null;
			String sModelRev = null;
			StringList slBusSelects = new StringList();
			slBusSelects.add("physicalid");
			slBusSelects.add(DomainObject.SELECT_REVISION);
			
			Iterator itrCFKey = mapFeatures.keySet().iterator();
			List<String[]> objectDataList = new ArrayList();
			//Map treeMap = new TreeMap(mapModelVSM);
			//String strVSM = treeMap.keySet().toString();
			//strVSM = strVSM.replace("[","");
			//strVSM = strVSM.replace("]", "");
			
			//Added for Incident 17457 : rkakde : Start
			String[] saVSMName = strVSM.split(",");
			for(int i=0; i<saVSMName.length;i++){
				sVSMKey = saVSMName[i];
				sVSMKey = sVSMKey.trim();
				if(UIUtil.isNullOrEmpty(strVSMDesc)){
					strVSMDesc = (String)mpVSMDetail.get(sVSMKey);
				}else{
					strVSMDesc = strVSMDesc +","+ mpVSMDetail.get(sVSMKey);
				}
				
			}
			//Added for Incident 17457 : rkakde : End
			objectDataList.add(new String[] { "","","","","",strVSMDesc});

			int i =1;		
			int iVSM = mapModelVSM.size();
			while(i<iVSM){
				strChar = strChar+",";
				i++;
			}
			
			//Added for Incident 17457 : rkakde : Start
			StringList slVSMCount = new StringList(iVSM);
			int j=0;
			while(j<iVSM){
				slVSMCount.add("0");
				j++;
			}
			//Added for Incident 17457 : rkakde : End
			
			objectDataList.add(new String[] { "Level","Feature Name","Feature","Effect In","Effect Out",strChar,"Feature Count"});
			StringList slVSMName = FrameworkUtil.split(strVSM, ",");
			while (itrCFKey.hasNext()) {
				strFeatureId = (String) itrCFKey.next();
				MapList mlCO = (MapList)mapFeatures.get(strFeatureId);
				Map mlCFDetails = (Map)mpGFD_ConfFeatures.get(strFeatureId);
				String strCFDisplayName = (String) mlCFDetails.get("DisplayName");
				if(strCFDisplayName.contains(",")){
					strCFDisplayName = strCFDisplayName.replace(",", ";");
				}
				String strCFName = (String) mlCFDetails.get("name");
				if(strCFName.contains(",")){
					strCFName = strCFName.replace(",", ";");
				}

				int iCOsize = mlCO.size();
				int iCOCount = 0;
				List<String[]> coDataList = new ArrayList();
				for(int coItr=0; coItr<iCOsize; coItr++){
					Map coMap = (Map)mlCO.get(coItr);
					strCOId = (String)coMap.get("id");
					strEffctivityEffectivity = (String)coMap.get("ActualEffectivity");
					
					Map mpCFDetails = (Map)mpGFD_ConfFeatures.get(strFeatureId);
					//Added for Incident 18192 : rkakde : Start
					//strEffectIn = getEffectivityInPoints(context,strEffctivityEffectivity,sModelName);
					//strEffectOut = getEffectivityOutPoints(context,strEffctivityEffectivity,sModelName);
					MapList mpModel = DomainObject.findObjects(context, "Model", sModelName , "*","*","*", null, false, slBusSelects);
					if ((mpModel != null) && (mpModel.size() > 0))
					{
						Map mpModelName = (Map)mpModel.get(0);
						sModelPhysicalId = (String)mpModelName.get("physicalid");
						sModelRev = (String)mpModelName.get(DomainObject.SELECT_REVISION);
						strEffectivityPoints = splitExpression(context,strEffctivityEffectivity,sModelPhysicalId);
						if(UIUtil.isNotNullAndNotEmpty(strEffectivityPoints)){
							saEffectInAndOutPoints = strEffectivityPoints.split(",");
							strEffectIn = saEffectInAndOutPoints[0];
							strEffectOut = saEffectInAndOutPoints[1];
						}

					}//Added for Incident 18192 : rkakde : End
					
					if(UIUtil.isNotNullAndNotEmpty(strEffectIn)){
						String strCODisplayName = (String) coMap.get("DisplayName");
						if(strCODisplayName.contains(",")){
							strCODisplayName = strCODisplayName.replace(",", ";");
						}
						String strCOName = (String) coMap.get("name");
						if(strCOName.contains(",")){
							strCOName = strCOName.replace(",", ";");
						}
						String[] saObj  = new String[] { "2",strCODisplayName,strCOName +" "+strCFName,strEffectIn,strEffectOut};
						String strObj = Arrays.toString(saObj);
	
						if(strObj.startsWith("[")){
							strObj = strObj.replace("[","");
						}
						if(strObj.endsWith("]")){
							strObj = strObj.replace("]", "");
						}
						int iOptVSM = 0;
						for(int vsmItr=0;vsmItr<slVSMName.size();vsmItr++){
							String strVSMKey = (String)slVSMName.get(vsmItr);
							strVSMKey = strVSMKey.trim();
							//Added for Incident 17457 : rkakde : Start
							String strVSMCO = (String)mapModelVSM.get(strVSMKey);	
							if(UIUtil.isNotNullAndNotEmpty(strVSMCO) && strVSMCO.contains(strCOId)){
								strObj = strObj + ",X";
								iOptVSM++;
								iCOCount++;
								slVSMCount.set(vsmItr,Integer.toString(Integer.parseInt((String)slVSMCount.get(vsmItr))+1));
								
							//Added for Incident 17457 : rkakde : End
							}else{
								strObj = strObj + ",";
							}
						}
						if(iVSM == 0){
							strObj = strObj + ",";
						}
						strObj = strObj + ","+Integer.toString(iOptVSM);
						//Added for Incident 17457 : rkakde : Start
						coDataList.add(new String[] {strObj});
						
					}
				}
				objectDataList.add(new String[] { "1",strCFDisplayName,strCFName,"","",strChar,Integer.toString(iCOCount)});
				objectDataList.addAll(coDataList);
			}
			
			int k =0;
			String strVSMCount = ",,,,";
			while(k<iVSM){
				strVSMCount=strVSMCount+","+slVSMCount.get(k);
				k++;
			}
			
			if (!objectDataList.isEmpty()) {
				objectDataList.add(new String[] {strVSMCount});
				writeactualreport(context, objectDataList,sModelName,sModelYear);
			}
			//Added for Incident 17457 : rkakde : End
		} catch (Exception e) {
			logger.info("ERROR " + e);
			e.printStackTrace();
		}
	}
	
	public void writeactualreport(Context context, List<String[]> objectDataList,String sModelName, String sModelYear)
			throws Exception
	{
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
			Date date1 = new Date();
			String strFileName;
			//Added for Incident 17457 : rkakde : Start
			if(UIUtil.isNotNullAndNotEmpty(sModelYear))
				strFileName = "/" + sModelName+"_"+sModelYear + "_VSM_Report.csv";
			else 
				strFileName = "/" + sModelName+"_VSM_Report.csv";
			//Added for Incident 17457 : rkakde : End
			FileWriter fos = new FileWriter(sReportPath+strFileName);
			PrintWriter dos = new PrintWriter(fos);
			int arrSize = objectDataList.size();
			int objCount = 0;
			String strData = null;
			for (objCount = 0; objCount < arrSize; objCount++)
			{
				dos.println();
				String[] data = (String[])objectDataList.get(objCount);
				strData = Arrays.toString(data);
				if(strData.startsWith("[")){
					strData = strData.replace("[", "");
				}
				if(strData.endsWith("]")){
					strData = strData.replace("]", "");
				}
				if(UIUtil.isNotNullAndNotEmpty(strData)){
					dos.print(strData);	
				}
			}
			dos.close();
			fos.close();
			System.out.println("File Written Successfully");
		} catch (Exception e) {
			logger.info("ERROR " + e);
			e.printStackTrace();
		}
	}
	
	/*public String getEffectivityInPoints(Context context, String strEffectivityExpression,String sModelName)throws Exception
	{ 
		String EffectivityInnName="";
		String[] saEffectivityIndexs= null;
		int iCount;
		int iSize;
		StringList selectables = new StringList();
		selectables.addElement(DomainConstants.SELECT_NAME);
		selectables.addElement(DomainConstants.SELECT_DESCRIPTION);
		String strMilestoneHolderId=null;
		String strModelName=null;
		StringList slLogicalFeaturesRelSelects 		= new StringList();
		slLogicalFeaturesRelSelects.addElement(DomainConstants.SELECT_FROM_ID);

		try{
			if(strEffectivityExpression!=null && !strEffectivityExpression.equals(""))
			{
				strEffectivityExpression = strEffectivityExpression.trim();
				
				boolean isMilestoneApplied = strEffectivityExpression.contains("EF_PR");
				if(isMilestoneApplied)
				{
					saEffectivityIndexs= strEffectivityExpression.split("OR");
					iSize = ltEffctivityIndexs.length;
					
					for(iCount = 0 ; iCount<iSize ; iCount++)
					{	
						String EfectivityInnExpression=ltEffctivityIndexs[iCount].toString();
						EfectivityInnExpression = EfectivityInnExpression.trim();
						while(EfectivityInnExpression.startsWith("(")){
							EfectivityInnExpression = EfectivityInnExpression.replaceFirst("\\(", "");
							EfectivityInnExpression = EfectivityInnExpression.trim();
						}
						if(EfectivityInnExpression.contains("PHY@EF"))
						{
							int firstIndexOfBracket=EfectivityInnExpression.indexOf("(");
							int indexOfOpeningBracket=EfectivityInnExpression.indexOf("[");
							int indexOfClosingBracket=EfectivityInnExpression.indexOf("]");
							strMilestoneHolderId=EfectivityInnExpression.substring(firstIndexOfBracket+1,indexOfOpeningBracket).trim();
							strMilestoneHolderId=strMilestoneHolderId.replace("PHY@EF:", "").trim();
							if(null!=strMilestoneHolderId && !strMilestoneHolderId.equals(""))
							{
								logger.info("strMilestoneHolderId == "+strMilestoneHolderId);
								DomainObject domMilestoneHolderObj=new DomainObject(strMilestoneHolderId);
								strModelName=domMilestoneHolderObj.getInfo(context, "name");
								
								if(sModelName.equals(strModelName))
								{
									String EffectivitySubString=EfectivityInnExpression.substring(indexOfOpeningBracket+1,indexOfClosingBracket).trim();
									EffectivitySubString=EffectivitySubString.replace("PHY@EF:", "").trim();
									String EffectivityIn=EffectivitySubString.substring(0,EffectivitySubString.indexOf("-"));
									if(EffectivityIn.length()>1)
									{
										DomainObject iedomObject = new DomainObject(EffectivityIn.trim());
										Map valueMap = iedomObject.getInfo(context, selectables);
										EffectivityInnName=EffectivityInnName+" "+(String) valueMap.get(DomainConstants.SELECT_NAME);
									}
								}
							}
						}
					}
				}
			}
		}catch(Exception e)
		{
			logger.info("ERROR ==== " + e.getLocalizedMessage());
			e.printStackTrace();
		}	
		finally{
			//ContextUtil.popContext(context);	
		}
		return EffectivityInnName.trim();
	}
	
	
	public String getEffectivityOutPoints(Context context, String strEffectivityExpression,String sModelName)throws Exception
	{ 
		String EffectivityOutName="";
		String[] saEffectivityIndexs= null;
		int iCount;
		int iSize;
		StringList selectables = new StringList();
		selectables.addElement(DomainConstants.SELECT_NAME);
		selectables.addElement(DomainConstants.SELECT_DESCRIPTION);
		String strMilestoneHolderId=null;
		String strModelName=null;
		StringList slLogicalFeaturesRelSelects 		= new StringList();
		slLogicalFeaturesRelSelects.addElement(DomainConstants.SELECT_FROM_ID);

		try{
			if(strEffectivityExpression!=null && !strEffectivityExpression.equals(""))
			{
				boolean isMilestoneApplied = strEffectivityExpression.contains("EF_PR");
				if(isMilestoneApplied)
				{
					saEffectivityIndexs= strEffectivityExpression.split("OR");
					iSize = ltEffctivityIndexs.length;
					
					for(iCount = 0 ; iCount<iSize ; iCount++)
					{
						String EfectivityInnExpression=ltEffctivityIndexs[iCount].toString();
						EfectivityInnExpression = EfectivityInnExpression.trim();
						while(EfectivityInnExpression.startsWith("(")){
							EfectivityInnExpression = EfectivityInnExpression.replaceFirst("\\(", "");
							EfectivityInnExpression = EfectivityInnExpression.trim();
						}
						if(EfectivityInnExpression.contains("PHY@EF"))
						{
							int firstIndexOfBracket=EfectivityInnExpression.indexOf("(");
							int indexOfOpeningBracket=EfectivityInnExpression.indexOf("[");
							int indexOfClosingBracket=EfectivityInnExpression.indexOf("]");
							strMilestoneHolderId=EfectivityInnExpression.substring(firstIndexOfBracket+1,indexOfOpeningBracket).trim();
							strMilestoneHolderId=strMilestoneHolderId.replace("PHY@EF:", "").trim();
							if(null!=strMilestoneHolderId && !strMilestoneHolderId.equals(""))
							{
								logger.info("strMilestoneHolderId == "+strMilestoneHolderId);
								DomainObject domMilestoneHolderObj=new DomainObject(strMilestoneHolderId);
								strModelName=domMilestoneHolderObj.getInfo(context, "name");
								
								if(sModelName.equals(strModelName))
								{
									String EffectivitySubString=EfectivityInnExpression.substring(indexOfOpeningBracket+1,indexOfClosingBracket).trim();
									EffectivitySubString=EffectivitySubString.replace("PHY@EF:", "").trim();
									String EffectivityIn=EffectivitySubString.substring(0,EffectivitySubString.indexOf("-"));
									if(EffectivitySubString.contains("^"))
									{
										EffectivityOutName=EffectivityOutName+" INF";
									}else
									{
										String EffectivityOut=EffectivitySubString.substring(EffectivitySubString.indexOf("-")+1,EffectivitySubString.length());
										if(EffectivityOut.length()>1)
										{
											DomainObject oedomObject = new DomainObject(EffectivityOut.trim());
											Map valueMap = oedomObject.getInfo(context, selectables);
											EffectivityOutName=EffectivityOutName+" "+(String) valueMap.get(DomainConstants.SELECT_NAME);
										}
									}
								}
							}
						}
					}
				}
			}
		}catch(Exception e)
		{
			logger.info("ERROR ==== " + e.getLocalizedMessage());
			e.printStackTrace();
		}	
		finally{
			//ContextUtil.popContext(context);	
		}
		return EffectivityOutName.trim();
	}*/
	//Added for Incident 18192 : rkakde : Start
	public String splitExpression(Context context,String strEffectivityExpression,String sModelPhysicalID)throws Exception
	{
		String[] saEffectivityIndexs= null;
		String[] ltANDNOTExpression = null;
		String[] ltEffectedINExpression = null;
		String[] ltEffectedOUTExpression = null;
		String strEffectedOutPoints = "";

		StringList slEffectedINList = new StringList();
		StringList slEffectedOUTList= new StringList();
		String strOREffectExpr= null;
		String strEffectExpr=null;

		try{

		if(UIUtil.isNotNullAndNotEmpty(strEffectivityExpression))
		{
			strEffectivityExpression = strEffectivityExpression.trim();

				saEffectivityIndexs= strEffectivityExpression.split("\\) OR \\(");
				int iSize = saEffectivityIndexs.length;
				
				for(int iCount = 0 ; iCount<iSize ; iCount++)
				{	
						strEffectExpr=saEffectivityIndexs[iCount].toString();
						strEffectExpr = strEffectExpr.trim();
																
						if( iCount == 0 && strEffectExpr.startsWith("(") ){											
							strEffectExpr = strEffectExpr.substring(1,strEffectExpr.length());
						} else if( (iCount == saEffectivityIndexs.length - 1) && (strEffectExpr.endsWith(")")) ){
							strEffectExpr = strEffectExpr.substring(0,strEffectExpr.length()-1);											
						}
						if( strEffectExpr.contains(":"+sModelPhysicalID+"[") ){
						if(strEffectExpr.contains("AND NOT"))
						{
							ltANDNOTExpression = strEffectExpr.split("\\) AND NOT \\(");	
							int iANDNOTSize = ltANDNOTExpression.length;								

							for(int iANCount = 0 ; iANCount<iANDNOTSize ; iANCount++)
							{								
								if(iANCount==0){
									strOREffectExpr=ltANDNOTExpression[iANCount].toString();
									if(strOREffectExpr.contains(" OR ")){
										ltEffectedINExpression = strOREffectExpr.split(" OR ");
										
										for(int iEffINCount = 0 ; iEffINCount<ltEffectedINExpression.length ; iEffINCount++)
										{
											if( !slEffectedINList.contains(ltEffectedINExpression[iEffINCount].toString()) ){
									
												slEffectedINList.add(ltEffectedINExpression[iEffINCount].toString());
											}
										}
											
									}else {
										if( !slEffectedINList.contains(strOREffectExpr) ){
											
												slEffectedINList.add(strOREffectExpr);
											}
									}
								}else {
									
									String strANDNOTEffectExpr=ltANDNOTExpression[iANCount].toString();
									if(strANDNOTEffectExpr.contains(" OR ")){
										ltEffectedOUTExpression = strANDNOTEffectExpr.split(" OR ");
										for(int iEffOUTCount = 0 ; iEffOUTCount<ltEffectedOUTExpression.length ; iEffOUTCount++)
										{
											if( !slEffectedOUTList.contains(strANDNOTEffectExpr)){
												
												slEffectedOUTList.add(ltEffectedOUTExpression[iEffOUTCount].toString());
											}
										}
									}else {
										if( !slEffectedOUTList.contains(strANDNOTEffectExpr) ){
											
												slEffectedOUTList.add(strANDNOTEffectExpr);
											}
									}
								}

							}
						}else{
								if(strEffectExpr.contains(" OR ")){
									ltEffectedINExpression = strEffectExpr.split(" OR ");
									for(int iEffINCount = 0 ; iEffINCount<ltEffectedINExpression.length ; iEffINCount++)
									{
										if( !slEffectedINList.contains(ltEffectedINExpression[iEffINCount].toString()) )
										{								
											slEffectedINList.add(ltEffectedINExpression[iEffINCount].toString());
										}
									}
										
								}else {
									if( !slEffectedINList.contains(strEffectExpr) ){
										
											slEffectedINList.add(strEffectExpr);
										}
								}
						
						}
					}
				}

				strEffectedOutPoints = processInAndOutList(context,slEffectedINList,slEffectedOUTList);
		}
	}catch(Exception e)
	{
		logger.info("ERROR ==== " + e.getLocalizedMessage());
		e.printStackTrace();
	}	
		return strEffectedOutPoints;

	}
	
	

	public static String processInAndOutList(Context context, StringList slEffectedINList,StringList slEffectedOUTList)throws Exception
	{
		String strPoints="";
		String strInPoints = "";
		String strOutPoints="";
		String strFinalOutPoints="";
		String strFinalInPoints="";
		Map mpInPoints = null;
		String strInPointsPhysicalId = "";
		try{
			
			if (slEffectedINList != null && slEffectedINList.size() > 0) 
			{
				for(int j=0; j<slEffectedINList.size(); j++)
				{
					String strOREffectivity = (String) slEffectedINList.get(j);
					strInPointsPhysicalId = getEffectivityInAndOutPoints(context,strOREffectivity);
					
					mpInPoints = getEffectInAndOutNames(context,strInPointsPhysicalId);
					if(mpInPoints!=null)
					{
						strInPoints = (String)mpInPoints.get(DomainConstants.SELECT_NAME);
						strFinalInPoints = strFinalInPoints + " " + strInPoints;
					}
					if(slEffectedOUTList!=null){
						strOutPoints =  getEffectivityOUTPoints(context,strInPointsPhysicalId,slEffectedOUTList);
						if(UIUtil.isNotNullAndNotEmpty(strOutPoints)){
							strFinalOutPoints = strFinalOutPoints +" "+strOutPoints;
						}else{
							strOutPoints = "INF";
							strFinalOutPoints = strFinalOutPoints +" "+strOutPoints;
						}
					}else{
						strOutPoints = "INF";
						strFinalOutPoints = strFinalOutPoints +" "+strOutPoints;
					}

				}
			}
			if(UIUtil.isNotNullAndNotEmpty(strFinalInPoints) && UIUtil.isNotNullAndNotEmpty(strFinalOutPoints) ){
				strPoints = strFinalInPoints + ","+strFinalOutPoints;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return strPoints;
	}
	
	
	public static String getEffectivityOUTPoints(Context context, String strEffectivityPhyID,StringList slEffectedOUTList)throws Exception
	{
		String strEffectOUTPoint = null;
		String strFinalEffectOUTPoint ="";
		String strEffectOUTPointName = "";
	   try{
		   String strExpandDetails = null;
		   String[] strHPDetails =null;
		   StringList slExpandDetails= new StringList();
		   String strHPPysicalID = null;
		   String strOUTPoints = null;
		   StringList slOUTPoints = new StringList();

		   String strCommand = "expand bus $1 from rel $2 recurse to rel select bus $3 dump $4";
    	   String strOutput = MqlUtil.mqlCommand(context, strCommand, strEffectivityPhyID,"Main Derived","physicalid","|");
		   int iOutCount = slEffectedOUTList.size();
		   for(int j =0;j<iOutCount;j++)
		   {
				String strANDNOTEffectivity = (String) slEffectedOUTList.get(j);
				strOUTPoints = getEffectivityInAndOutPoints(context,strANDNOTEffectivity);
				if( !slOUTPoints.contains(strOUTPoints) )
				{
					slOUTPoints.add(strOUTPoints);
				}
		   }
		   if(UIUtil.isNotNullAndNotEmpty(strOutput))
		   {
				slExpandDetails = StringUtil.split(strOutput, "\n");
				int iCount = slExpandDetails.size();
				for(int i =0;i<iCount;i++)
				{
					strExpandDetails = (String) slExpandDetails.get(i);
					strHPDetails = strExpandDetails.split("\\|");
					strHPPysicalID = (String) strHPDetails[6];
					strEffectOUTPoint = (String) strHPDetails[5];
					strEffectOUTPointName = (String) strHPDetails[4];
					if(slOUTPoints.contains(strHPPysicalID))
					{
						strFinalEffectOUTPoint = strEffectOUTPointName;
					}
				}

			}
	   }catch(Exception e)
	   {
		   e.printStackTrace();
	   }
	   return strFinalEffectOUTPoint;
   }
	
	public static Map getEffectInAndOutNames(Context context,String EffectivityModelPhysicalID )throws Exception
	{
		Map valueMap = null;
		try
		{
			StringList selectables = new StringList();
			selectables.addElement(DomainConstants.SELECT_NAME);
			selectables.addElement(DomainConstants.SELECT_REVISION);
			if(UIUtil.isNotNullAndNotEmpty(EffectivityModelPhysicalID))
			{

				DomainObject oedomObject = new DomainObject(EffectivityModelPhysicalID.trim());
				valueMap = oedomObject.getInfo(context, selectables);

			}

		
		}catch(Exception e){
			e.printStackTrace();
		}
		return valueMap;
	}
   
	
	public static String getEffectivityInAndOutPoints(Context context, String strEffectivityExpression)throws Exception
	{ 

		String[] saEffectivityIndexs= null;
		int iCount;
		int iSize;
		StringList selectables = new StringList();
		selectables.addElement(DomainConstants.SELECT_NAME);
		selectables.addElement(DomainConstants.SELECT_DESCRIPTION);
		String strMilestoneHolderId=null;
		StringList slLogicalFeaturesRelSelects 		= new StringList();
		slLogicalFeaturesRelSelects.addElement(DomainConstants.SELECT_FROM_ID);
		String EffectivityModelPhysicalID ="";

		try{

			if(UIUtil.isNotNullAndNotEmpty(strEffectivityExpression))
			{
				strEffectivityExpression = strEffectivityExpression.trim();
				
				boolean isMilestoneApplied = strEffectivityExpression.contains("EF_PR");
				if(isMilestoneApplied)
				{
					saEffectivityIndexs= strEffectivityExpression.split("OR");
					iSize = saEffectivityIndexs.length;
					
					for(iCount = 0 ; iCount<iSize ; iCount++)
					{	
						String EfectivityInnExpression=saEffectivityIndexs[iCount].toString();
						EfectivityInnExpression = EfectivityInnExpression.trim();
						while(EfectivityInnExpression.startsWith("(")){
							EfectivityInnExpression = EfectivityInnExpression.replaceFirst("\\(", "");
							EfectivityInnExpression = EfectivityInnExpression.trim();
						}
						if(EfectivityInnExpression.contains("PHY@EF"))
						{
							int firstIndexOfBracket=EfectivityInnExpression.indexOf("(");
							int indexOfOpeningBracket=EfectivityInnExpression.indexOf("[");
							int indexOfClosingBracket=EfectivityInnExpression.indexOf("]");
							strMilestoneHolderId=EfectivityInnExpression.substring(firstIndexOfBracket+1,indexOfOpeningBracket).trim();
							strMilestoneHolderId=strMilestoneHolderId.replace("PHY@EF:", "").trim();

							if(UIUtil.isNotNullAndNotEmpty(strMilestoneHolderId))
							{
								String EffectivitySubString=EfectivityInnExpression.substring(indexOfOpeningBracket+1,indexOfClosingBracket).trim();
								EffectivitySubString=EffectivitySubString.replace("PHY@EF:", "").trim();
								EffectivityModelPhysicalID=EffectivitySubString.substring(0,EffectivitySubString.indexOf("-"));
								
							}
						}
					}
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
		}	
		finally{
			//ContextUtil.popContext(context);	
		}
		return EffectivityModelPhysicalID;
	}
	
	//Added for Incident 18192 : rkakde : End
	public static Comparator<String> c = new Comparator<String>() {
	    @Override
	    public int compare(String object1, String object2) {
	        Matcher m = p.matcher(object1);
	        Integer number1 = null;
	        if (!m.find()) {
	            return object1.compareTo(object2);
	        }
	        else {
	            Integer number2 = null;
	            number1 = Integer.parseInt(m.group());
	            m = p.matcher(object2);
	            if (!m.find()) {
	                return object1.compareTo(object2);
	            }
	            else {
	                number2 = Integer.parseInt(m.group());
	                int comparison = number1.compareTo(number2);
	                if (comparison != 0) {
	                    return comparison;
	                }
	                else {
	                    return object1.compareTo(object2);
	                }
	            }
	        }
	    }
	};
	
	

}